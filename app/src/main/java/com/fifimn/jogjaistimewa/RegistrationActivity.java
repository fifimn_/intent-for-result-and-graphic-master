package com.fifimn.jogjaistimewa;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class RegistrationActivity extends AppCompatActivity {

    private EditText userName, userPassword, userEmail, userPhone;
    private Button regButton;
    private TextView userLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        setupUIViewss();

        regButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validate()) {
                    Intent in = new Intent(RegistrationActivity.this, LoginActivity.class);
                    in.putExtra("email", userEmail.getText().toString());
                    in.putExtra("password", userPassword.getText().toString());
                    startActivity(in);
                }

            }
        });

         userLogin.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 Intent in = new Intent(RegistrationActivity.this, LoginActivity.class);
                 startActivity(in);
             }
         });
    }

    private void setupUIViewss(){
        userName = (EditText)findViewById(R.id.editNama);
        userPassword = (EditText)findViewById(R.id.editPW);
        userEmail = (EditText)findViewById(R.id.editEmail);
        userPhone = (EditText)findViewById(R.id.editPhone);
        regButton = (Button)findViewById(R.id.button_Regis);
        userLogin = (TextView)findViewById(R.id.R_Login);
    }
    private Boolean validate(){
        Boolean result = false;

        String name= userName.getText().toString();
        String password = userPassword.getText().toString();
        String email = userEmail.getText().toString();
        String phone = userPhone.getText().toString();

        if(name.isEmpty() && password.isEmpty() && email.isEmpty() && phone.isEmpty()){
            Toast.makeText(this, "Please enter all the details", Toast.LENGTH_SHORT).show();
        }else{
            result = true;
        }
        return result;


    }

}
