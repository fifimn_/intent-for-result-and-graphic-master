package com.fifimn.jogjaistimewa;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class LoginActivity extends AppCompatActivity {

    private Button login;
    private TextView regis;
    private EditText txtPassword, txtEmail;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        txtEmail = findViewById(R.id.Email);
        txtPassword = findViewById(R.id.Password);
        login = findViewById(R.id.Login_Button);
        regis = findViewById(R.id.Register);

        Intent in = getIntent();
        Bundle bundle = in.getExtras();

        if(bundle != null) {
            txtEmail.setText((String) bundle.get("email"));
            txtPassword.setText((String) bundle.get("password"));
        }

//        if (txtEmail.getText().toString() != null && txtPassword.getText().toString() != null ) {
//            startActivity(LoginActivity.this, AppActivity.class);
//        }


        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.d("LoginActivity", txtEmail.getText().toString());
                Log.d("LoginActivity", txtPassword.getText().toString());
                if (!txtPassword.getText().toString().equals("") && !txtEmail.getText().toString().equals("")) {
                    startActivity(new Intent(LoginActivity.this, AppActivity.class));
                }
                Intent intent = new Intent(LoginActivity.this, AppActivity.class);
                startActivityForResult(intent, 99);


            }
        });

        regis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this,RegistrationActivity.class));
            }
        });



    }

//    private void startActivity(LoginActivity loginActivity, Class<AppActivity> appActivityClass) {
//    }
}
