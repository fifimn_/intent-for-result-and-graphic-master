package com.fifimn.jogjaistimewa;

import android.graphics.ColorFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.widget.ImageView;

import static android.graphics.Bitmap.Config.ARGB_8888;

public class AppActivity extends AppCompatActivity {
    ImageView ourView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        draw();
        setContentView(ourView);
    }
    public void draw(){


        Bitmap blankBitmap;
        blankBitmap = Bitmap.createBitmap(600,600, ARGB_8888);
        Canvas canvas;
        canvas = new Canvas(blankBitmap);

        ourView = new ImageView(this);
        ourView.setImageBitmap(blankBitmap);

        Paint paint;
        paint = new Paint();

        // Memberikan warna kanvas
        canvas.drawColor(Color.argb(150, 230, 230, 250));

        // Mengatur warna
        paint.setColor(Color.argb(150,  30, 144, 255));
        // We can change this around as well


        Bitmap bitmapGambar;
        // insialisasi gambar
        bitmapGambar = BitmapFactory.decodeResource(this.getResources(), R.drawable.segi3);

        // Posisi gambar
        canvas.drawBitmap(bitmapGambar, 100, 100, paint);

        // membuat lingkaran


        paint.setColor(Color.rgb(255,69,0));
        canvas.drawCircle(200,300,80,paint);

        canvas.drawCircle(250,400,80,paint);

        canvas.drawCircle(370,370,80,paint);
        canvas.drawCircle(400,270,80,paint);
        canvas.drawCircle(350,190,80,paint);
        canvas.drawCircle(225,185,80,paint);

        canvas.drawLine(500,300,600,400,paint);
        canvas.drawLine(50,300,200,400,paint);

        paint.setColor(Color.rgb(255,255,0));
        canvas.drawCircle(300,300,80,paint);


        // mengubah warna
        paint.setColor(Color.argb(200 ,  188, 143, 143));

        // membuar kotak
        canvas.drawRect(0,400,350,400,paint);

    }
}
